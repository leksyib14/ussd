
const data = [
  { trackingNumber: 'ZN0878333', status: 'Delivered to Texas' },
  { trackingNumber: 'ZN0878334', status: 'Enroute to massachuesets' },
  { trackingNumber: 'ZN0878335', status: 'Picked up' },
]

const businesses = [
  {
    email: 'favour@yahoo.com',
    secret: '72hUHd3r',
    months: {
      january: {
        income: '$30,000',
        expenses: '$15,000'
      },
      february: {
        income: '$50,000',
        expenses: '$9,000'
      },
      march: {
        income: '$50,000',
        expenses: '$9,000'
      },
    }
  },
  {
    email: 'favour2@yahoo.com',
    secret: '72hUHd3r2',
    months: {
      january: {
        income: '$30,000',
        expenses: '$15,000'
      },
      february: {
        income: '$50,000',
        expenses: '$9,000'
      },
      march: {
        income: '$50,000',
        expenses: '$9,000'
      },
    }
  }
]

const formatStats = (months) => {
  const entries = Object.entries(months)

  return `${entries.map(e => `${e[0]} income: ${e[1].income}, expenses: ${e[1].expenses}`)}`
}

class Controller {
  trackPackage = (req, res) => {
    const body = { ...req.body }
    if (body.text === '') {
      return res.status(200).send(`CON Enter tracking number.`)
    }
    const item = data.find(item => item.trackingNumber === body.text)

    if (item) return res.status(200).send(`END ${item.status}`)

    return res.send('END item not found')
  }

  getBusinessDetails = (req, res) => {
    const body = { ...req.body }
    if (body.text === '') {
      return res.status(200).send(`CON Enter email and secret. seperate with a comma`)
    }

    const [email, secret] = body.text.split(',')
    const business = businesses.find(b => b.email === email.trim() && b.secret === secret.trim())

    if (business) return res.status(200).send(`END ${formatStats(business.months)}`)

    return res.status(404).send('END user not found')
  }
}

module.exports = Controller
