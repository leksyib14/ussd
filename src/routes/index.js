const express = require('express')
const router = express.Router()
const Controller = require('../controllers')

const controller = new Controller()

router.post('/track', controller.trackPackage)
router.post('/stats', controller.getBusinessDetails)

module.exports = router