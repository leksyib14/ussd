const express = require('express')
const bodyparser = require('body-parser')
const router = require('./src/routes')

const app = express()
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: true }))
app.use('/', router)

const port = process.env.PORT || 4000

app.listen(port, () => {
  console.log(`server's running on ${port}`)
})
